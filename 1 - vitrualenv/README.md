# Python開發必備：virtualenv
> ## 安裝virtualenv
* [virtualenv](https://virtualenv.pypa.io/en/stable/)是一個第三方包，是管理虛擬境的常用方法。Python 3 也有自帶套件管理工具，如：pip。
```bash
開啟console(命令提示字元)  >
輸入 pip install virtualenv [ Enter] >
```
> 使用方法
```bash
1.建立乾淨環境(venv)
開啟console(命令提示字元)  >
輸入 D:  [Enter] >
輸入virtualenv venv [Enter] >
```

```bash
2.建立編輯器環境(jupyter notebook)
pip install jupyter [Enter] >
jupyter notebook --ip 0.0.0.0 --port 8888
電腦將會動開啟瀏覽器與編輯器環器
```